import React from 'react';
import { Link } from 'react-router-dom';

import { Layout, Menu } from 'antd';
import 'antd/dist/antd.css';

import './NavigationBar.scss';

const NavigationBar = () => {
    const { Header } = Layout;
    return (
        <Header className="header">
            <div className="logo">
                <Link to="/">YourNRG</Link>
            </div>
            <Menu
                theme="dark"
                mode="horizontal"
                defaultSelectedKeys={['0']}
                style={{ lineHeight: '64px' }}
            >
                <Menu.Item key="1">
                    <Link to="/electricity-consumption">Electricity Consumption</Link>
                </Menu.Item>
                <Menu.Item key="2">
                    <Link to="/water-consumption">Water Consumption</Link>
                </Menu.Item>
                <Menu.Item key="3">
                    <Link to="/gas-consumption">Gas Consumption</Link>
                </Menu.Item>
                <Menu.Item key="4">
                    <Link to="/thermal-consumption">Thermal Consumption</Link>
                </Menu.Item>
                <Menu.Item key="5">
                    <Link to="/total-consumption">Total Consumption Data</Link>
                </Menu.Item>
            </Menu>
        </Header>
    )
}
export default NavigationBar;
