import React from 'react';
import { Link } from 'react-router-dom';

import { Menu, Icon, } from 'antd';
import 'antd/dist/antd.css';

import './MenuCustom.scss';

const MenuCustom = (props) => {

    const { SubMenu } = Menu;

    const { menuName } = props;
    const { handleClick, currentState, option1, option2, option3, option4 } = props;

    return (
        <Menu className="custom-menu-container"
            theme='dark'
            onClick={handleClick}
            defaultOpenKeys={['sub1']}
            selectedKeys={[currentState]}
            mode="inline"
        >
            <SubMenu
                key="sub11"
                title={
                    <span>
                        <Icon type="mail" />
                        <span>{menuName}</span>
                    </span>
                }
            >
                <Menu.Item key="11"><Link to="/recommendations">{option1}</Link></Menu.Item>
                <Menu.Item key="21"><Link to="/calculator">{option2}</Link></Menu.Item>
                <Menu.Item key="31">{option3}</Menu.Item>
                <Menu.Item key="41">{option4}</Menu.Item>
            </SubMenu>
        </Menu>
    )
}
export default MenuCustom;
