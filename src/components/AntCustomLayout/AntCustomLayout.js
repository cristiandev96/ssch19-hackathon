import React from 'react';
import { Switch, Route } from 'react-router-dom';
import { Layout, Breadcrumb } from 'antd';
import 'antd/dist/antd.css';

import { ElectricityData } from '../Consumption';
import { WaterData } from '../Consumption';
import { GasData } from '../Consumption';
import { ThermalData } from '../Consumption/';
import { TotalData } from '../Consumption';
import { NavigationBar } from '../NavigationBar';
import { MenuCustom } from '../MenuCustom';
import Recommendations from '../Pages/Recommendations/Recommendations';
import Calculator from '../Pages/Calculator/Calculator';
import './AntCustomLayout.scss';

const AntCustomLayout = () => {

   const { Content, Sider } = Layout;

   return (
      <div>
         <Layout theme="dark">
            <NavigationBar />
            <Layout>
               <Sider width={256} >
                  <MenuCustom
                     menuName='User Profile'
                     option1='option1'
                     option2='option2'
                     option3='option3'
                     option4='option4'
                  />
                  <MenuCustom
                     menuName='Savings'
                     option1='Recommendations'
                     option2='Calculator'
                     option3='option3'
                     option4='option4'
                  />
                  <MenuCustom
                     menuName='Total Data Info'
                     option1='option1'
                     option2='option2'
                     option3='option3'
                     option4='option4'
                  />
               </Sider>
               <Layout style={{ padding: '0 24px 24px' }}>
                  <Breadcrumb style={{ margin: '16px 0' }}>
                     <Breadcrumb.Item>Home</Breadcrumb.Item>
                     <Breadcrumb.Item>List</Breadcrumb.Item>
                     <Breadcrumb.Item>App</Breadcrumb.Item>
                  </Breadcrumb>
                  <Content
                     style={{
                        background: '#fff',
                        padding: 24,
                        margin: 0,
                        minHeight: 280,
                     }}
                  >
                     <Switch>
                        <Route path="/" exact />
                        <Route path="/electricity-consumption" component={ElectricityData} />
                        <Route path="/water-consumption" component={WaterData} />
                        <Route path="/gas-consumption" component={GasData} />
                        <Route path="/thermal-consumption" component={ThermalData} />
                        <Route path="/recommendations" component={Recommendations} />
                        <Route path="/calculator" component={Calculator} />
                        <Route path="/total-consumption" component={TotalData} />
                     </Switch>
                  </Content>
               </Layout>
            </Layout>
         </Layout>
      </div>
   )
}

export default AntCustomLayout;
