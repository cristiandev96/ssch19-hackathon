import React from 'react';
import { Doughnut } from 'react-chartjs-2';

import './WaterData.scss';

const WaterData = () => {

    const datas = {
        labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
        datasets: [{
            data: [300, 50, 100, 300, 50, 100, 300, 50, 100, 300, 50, 100],
            backgroundColor: [
                '#E9967A',
                '#FFFFF0',
                '#B0E0E6',
                '#FFEFD5',
                '#ADD8E6',
                '#708090',
                '#FF1493',
                '#F08080',
                '#8B4513',
                '#32CD32',
                '#BC8F8F',
                '#7FFF00'
            ],
            hoverBackgroundColor: [
                '#E9967A',
                '#FFFFF0',
                '#B0E0E6',
                '#FFEFD5',
                '#ADD8E6',
                '#708090',
                '#FF1493',
                '#F08080',
                '#8B4513',
                '#32CD32',
                '#BC8F8F',
                '#7FFF00'
            ]
        }]
    };

    return (
        <div>
            <h2>Water Consumption per years (2018-2019):</h2>
            <Doughnut
                data={datas}
                width={100}
                height={55}
            />
        </div>
    )
}
export default WaterData;
