import React from 'react';

import './GasData.scss';

import { Line } from 'react-chartjs-2';

const data = {
    labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
    datasets: [
        {
            label: 'Gas Consumption Stats',
            fill: false,
            lineTension: 0.37,
            backgroundColor: 'rgba(75,192,192,0.4)',
            borderColor: 'rgba(255, 152, 0,1)',
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.2,
            borderJoinStyle: 'miter',
            pointBorderColor: 'rgba(75,192,192,1)',
            pointBackgroundColor: '#fff',
            pointBorderWidth: 1,
            pointHoverRadius: 25,
            pointHoverBackgroundColor: 'rgba(75,192,192,1)',
            pointHoverBorderColor: 'rgba(220,220,220,1)',
            pointHoverBorderWidth: 5,
            pointRadius: 1,
            pointHitRadius: 15,
            data: [65, 59, 80, 81, 56, 55, 40, 65, 59, 80, 81, 56, 55, 40]
        }
    ]
};

const GasData = () => {
    return (
        <div>
            <h2>Gas Consumption per years (2018-2019):</h2>
            <Line
                data={data}
                width={100}
                height={55}
            />
        </div>
    )
}
export default GasData;
