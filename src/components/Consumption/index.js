export { default as GasData } from './GasData/GasData';
export { default as WaterData } from './WaterData/WaterData';
export { default as ElectricityData } from './ElectricityData/ElectricityData';
export { default as ThermalData } from './ThermalData/ThermalData';
export { default as TotalData } from './TotalData/TotalData';
