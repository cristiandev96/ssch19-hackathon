import React, { useEffect } from 'react';

import { Bar } from 'react-chartjs-2';
import { apiFetchAllDataRequest } from '../../../store/actions/fetchDataAction';
import { connect } from 'react-redux';

const ElectricityData = (props) => {

    const { apiFetchAllDataRequest } = props;

    const data = {
        labels: ['Noiembrie 2018', 'Decembrie 2018', 'Ianuarie 2019', 'Februarie 2019', 'Martie 2019', 'Aprilie 2019',
            'Mai 2019', 'Iunie 2019', 'Iulie 2019', 'August 2019', 'Septembrie 2019', 'Octombrie 2019'],
        datasets: [
            {
                label: 'Energy Consumption Stats',
                backgroundColor: 'rgba(255, 152, 0,0.3)',
                borderColor: 'rgba(255, 152, 0, 1)',
                borderWidth: 2,
                hoverBackgroundColor: 'rgba(214, 128, 0, 0.4)',
                hoverBorderColor: 'rgba(214, 128, 0, 0.5)',
                data: [4.21, 4.38, 4.33, 4.14, 4.07, 3.82, 4.17, 3.81, 3.59, 3.81, 3.41, 6.44]
            }
        ]
    };

    useEffect(() => {
        apiFetchAllDataRequest()

    }, [apiFetchAllDataRequest])


    return (
        <div>
            <div>
                <h2>Energy Consumption per years (2018-2019):</h2>
                <Bar
                    data={data}
                    width={100}
                    height={500}
                    options={{
                        maintainAspectRatio: false
                    }}
                />
            </div>
        </div>
    )
}

const mapStateToProps = state => ({
    fetchedData: state.fetchDataReducer.data.data
})

const mapDispatchToProps = {
    apiFetchAllDataRequest: apiFetchAllDataRequest
}

export default connect(mapStateToProps, mapDispatchToProps)(ElectricityData);
