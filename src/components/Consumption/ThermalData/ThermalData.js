import React from 'react';

import { HorizontalBar } from 'react-chartjs-2';

const ThermalData = () => {

    const data = {
        labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
        datasets: [
            {
                label: 'Thermal Consumption Stats',
                backgroundColor: 'rgba(255, 152, 0 ,0.2)',
                borderColor: 'rgba(255, 152, 0 ,1)',
                borderWidth: 1,
                hoverBackgroundColor: 'rgba(214, 128, 0, 0.4)',
                hoverBorderColor: 'rgba(214, 128, 0, 1)',
                data: [65, 59, 80, 81, 56, 55, 40, 56, 55, 40, 59, 80, 81]
            }
        ]
    };

    return (
        <div>
            <h2>Thermal Consumption per years (2018-2019):</h2>
            <HorizontalBar
                data={data}
                width={100}
                height={55} />
        </div>
    )
}
export default ThermalData;
