import React, { useState } from 'react';

import './Calculator.scss';

const Calculator = () => {

    const [calcMonth1, setCalcMonth1] = useState(0);
    const [calcMonth2, setCalcMonth2] = useState(0);
    const [calcYear1, setCalcYear1] = useState(0);
    const [calcYear2, setCalcYear2] = useState(0);

    const [inputValue1, setInputValue1] = useState('');
    const [inputValue2, setInputValue2] = useState('');
    const [inputValue3, setInputValue3] = useState('');
    const [inputValue4, setInputValue4] = useState('');
    const [inputValue5, setInputValue5] = useState('');
    const [inputValue6, setInputValue6] = useState('');
    const [inputValue7, setInputValue7] = useState('');
    const [inputValue8, setInputValue8] = useState('');

    let hourValue = 60;
    let yearValue = 365;
    let energyCost = 0.00189;

    // INPUTS IN ORDER

    const onChangeHandler1 = (e) => {
        setInputValue1(e.target.value)
    }

    const onChangeHandler2 = (e) => {
        setInputValue2(e.target.value)
    }

    const onChangeHandler3 = (e) => {
        setInputValue3(e.target.value)
    }
    const onChangeHandler4 = (e) => {
        setInputValue4(e.target.value)
    }
    const onChangeHandler5 = (e) => {
        setInputValue5(e.target.value)
    }

    const onChangeHandler6 = (e) => {
        setInputValue6(e.target.value)
    }

    const onChangeHandler7 = (e) => {
        setInputValue7(e.target.value)
    }
    const onChangeHandler8 = (e) => {
        setInputValue8(e.target.value)
    }

    // END INPUTS IN ORDER

    const calcMonthHandler1 = () => {
        setCalcMonth1(inputValue1 * inputValue2 * hourValue * energyCost);
    }

    const calcMonthHandler2 = () => {
        setCalcMonth2(inputValue3 * inputValue4 * hourValue * energyCost);
    }


    const calcYearHandler1 = () => {
        setCalcYear1(inputValue5 * inputValue6 * yearValue * energyCost);
    }

    const calcYearHandler2 = () => {
        setCalcYear2(inputValue7 * inputValue8 * yearValue * energyCost);
    }

    return (
        <div className="calculator-container">
            <div className="calculator-title">
                <h3>Calculator:</h3>
            </div>
            <div className="calculator-inner">
                <h3 className="calculator-inner-toptext">Economisire pe 2 luni <span> {`${(calcMonth1 - calcMonth2).toFixed()} lei`} -  Diferenta </span></h3>
                <p> <strong>Echipament vechi</strong> - Formula :
                    (<input onChange={onChangeHandler1} className="user-input-formula" type="text" />)P/ore
                    x
                    (<input onChange={onChangeHandler2} className="user-input-formula" type="text" />) ore/zi x {hourValue} x {energyCost} lei = ({(calcMonth1).toFixed(2)}) lei
                    <button onClick={calcMonthHandler1} className="calc-btn">Calculate</button>
                </p>
                <p> <strong>Echipament nou</strong> - Formula :
                    (<input onChange={onChangeHandler3} className="user-input-formula" type="text" />)P/ore
                    x
                    (<input onChange={onChangeHandler4} className="user-input-formula" type="text" />) ore/zi x {hourValue} x {energyCost} lei = ({(calcMonth2).toFixed(2)}) lei
                    <button onClick={calcMonthHandler2} className="calc-btn">Calculate</button>
                </p>
            </div>

            <div className="calculator-inner">
                <h3 className="calculator-inner-toptext" >Economisire pe 1 an <span> {`${(calcYear1 - calcYear2).toFixed()} lei`} -  Diferenta </span></h3>
                <p> <strong>Echipament vechi</strong> - Formula :
                    (<input onChange={onChangeHandler5} className="user-input-formula" type="text" />)P/ore
                    x
                    (<input onChange={onChangeHandler6} className="user-input-formula" type="text" />) ore/zi x {yearValue} x {energyCost}  lei = ({calcYear1}) lei
                    <button onClick={calcYearHandler1} className="calc-btn">Calculate</button>
                </p>
                <p> <strong>Echipament nou</strong> - Formula :
                    (<input onChange={onChangeHandler7} className="user-input-formula" type="text" />)P/ore
                    x
                    (<input onChange={onChangeHandler8} className="user-input-formula" type="text" />) ore/zi x {yearValue} x {energyCost}  lei = ({calcYear2}) lei
                    <button onClick={calcYearHandler2} className="calc-btn">Calculate</button>
                </p>
            </div>
            <div className="calculator-bottom-info">
                <h3>P/ore - puterea consumata pe o ora</h3>
                <h3>ore/zi - numarul de ore in zi</h3>
            </div>
        </div>
    )
}
export default Calculator;
