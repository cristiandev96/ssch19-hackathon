import React from 'react';
import lampAd from '../../../assets/lamp-ad.jpg';
import energStats from '../../../assets/energ-stats.jpg';
import './Recommendations.scss';

const Recommendations = () => {
    return (
        <div className="recommendation-container">
            <h2>Metode de economisire a consumului de electrică,termică,apă etc.</h2>
            <div className="recommendations-inner">
                <ul className="recommendations-list">
                    <li>- Schimbați toate becurile incandescente cu unele economice (fluorescente sau cu LED), care consumă de cinci ori mai puțină energie.</li>
                    <div className="recommendations-ads1">
                        <img src="https://newsdiaspora.ro/wp-content/uploads/2016/02/reclama.png" alt="" />
                    </div>
                    <li> - Deconectaţi lumina când ieşiţi din încăpere.</li>
                    <li> - Folosiți cât mai multă lumină naturală și nu aprinde becurile decât atunci când ai nevoie</li>
                    <li> - Instalați în afara casei sau în zonele intens circulate un senzor de mișcare, ce aprinde lumina numai atunci când acesta detectează mișcare.</li>
                    <li> - Electrocasnicele din clasa A (sau A+, A++) au un randament mai mare pentru mai puțini wați</li>
                    <div className="recommendations-ads2">
                        <img src={energStats} alt="img" />
                    </div>
                    <li> - Deconectaţi aparatele neutilizate de la sursa de energie. Nu le lăsaţi în regim de "Pauză" sau "Stand-by".</li>
                    <li> - Deconectați incărcătoarele atunci cand nu le folosiți</li>
                    <li> - Aveți grija de electrocasnice</li>
                    <li> - Înlocuiți televizorul și monitorul de calculator cu unele noi care utilizează tehnologie LED</li>
                    <li> - Folosiți laptop în loc de calculator obișnuit</li>
                    <li> - Folosiți  doar ce aveți nevoie</li>

                    <li> - Izolați casa(ușile, geamurile, pereții și tavanele)</li>
                    <li> -  O maşină de spălat îşi reduce consumul de electricitate cu o treime dacă se foloseşte un program de spălare la 40 grade °C în loc de 60 grade °C</li>
                    <li> - Poziţionaţi frigiderul departe de o sursă de căldură întrucât acesta poate consuma aproape dublu dacă stă aproape de calorifer sau aragaz.</li>
                    <li> - Alegeți să faceți duș în loc de baie</li>
                    <li> - Instalați un cap de duș cu debit scăzut și valoare facturilor va fi vizibil mai mică.</li>
                    <p>O familie formată din 4 membri poate economisi anual 160.000 de litri de apă și implicit energie electrică folosind o metodă simplă și puțin costisitoare.</p>
                    <li>Achiziționați un termostat</li>
                    <p>O soluție potrivită pentru costuri rezonabile este termostatul. Principiul de funcționare al acestuia este simplu: ai la dispoziție posibilitatea de a seta un orar de funcționare a centralei, precum și temperatura dorită, în funcție de anumite intervale de timp.</p>
                    <li> - Închide încălzirea</li>
                    <p>Dacă aveți camere pe care nu le folosiți în locuință, închideți ușile și opriți căldura. Astfel, veți economisi energia electrică care în mod normal s-ar consuma pentru menținerea temperaturii acelei încăperi.</p>
                    <li> - Folosiți cuptorul cu microunde</li>
                    <p>Cu toții știm că mâncarea poate fi reîncălzită foarte ușor folosind cuptorul electric, însă acest lucru consumă de două ori mai multă energie electrică față de cuptorul cu microunde și durează de două ori mai mult (preîncălzirea). Pentru a putea savura o cină caldă, folosiți cuptorul cu microunde.</p>
                    <li> - Utilizarea aragazului pentru încălzire este ineficientă şi periculoasă.</li>
                </ul>
            </div>
        </div>
    )
}

export default Recommendations;
