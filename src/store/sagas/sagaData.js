import { takeEvery, put, call } from 'redux-saga/effects';
import { API_FETCH_ALL_DATA_REQUEST, API_FETCH_ALL_DATA_SUCCESS } from '../types/index';

function requestData() {
    return fetch('https://consumeter.verily.ovh/consumption/get')
        .then(res => res.json());
}

function* fetchAllData() {
    const data = yield call(requestData)

    yield put({
        type: API_FETCH_ALL_DATA_SUCCESS,
        payload: {
            isLoading: false,
            data
        }
    })
}

export function* watchFetchAllData() {
    yield takeEvery(API_FETCH_ALL_DATA_REQUEST, fetchAllData)
}
