import { API_FETCH_ALL_DATA_SUCCESS, API_FETCH_ALL_DATA_REQUEST } from '../types/index';

export const apiFetchAllData = (payload) => ({
    type: API_FETCH_ALL_DATA_SUCCESS,
    payload
})

export const apiFetchAllDataRequest = (payload) => ({
    type: API_FETCH_ALL_DATA_REQUEST,
    payload
})
