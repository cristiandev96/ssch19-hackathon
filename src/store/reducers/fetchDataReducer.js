import { API_FETCH_ALL_DATA_SUCCESS, API_FETCH_ALL_DATA_REQUEST } from '../types/index';

const initialState = {
    data: [],
    isLoading: null
}

export default function (state = initialState, action) {
    switch (action.type) {
        case API_FETCH_ALL_DATA_REQUEST:
            return {
                ...state,
                isLoading: true
            }
        case API_FETCH_ALL_DATA_SUCCESS:
            return {
                ...state,
                data: action.payload,
                isLoading: false
            }
        default:
            return state;
    }
}
