import React from 'react';

import { AntCustomLayout } from './components/AntCustomLayout';

const App = () => {
   return (
      <div className="app-container">
         <AntCustomLayout />
      </div>
   )
}
export default App;
